import React, { Component } from 'react'
import axios from 'axios'
import './App.css'
const URL = document.location.host === 'localhost:3000' ? 'localhost:3001' : 'setelorders-env.vedgpa35tu.us-east-2.elasticbeanstalk.com'

class App extends Component {
  state = {
    name: '',
    description: '',
    list: [],
    orders: []
  }

  componentDidMount = async () => {
    try {
      const request = await axios.get(`http://${URL}/api`)
      const { data } = request
      this.setState({ list: data }, () => this.renderOrders())
    } catch (e) {
      console.log('Error')
    }
  }

  update = evt => {
    this.setState({
      [evt.target.id]: evt.target.value
    })
  }

  renderOrders = () => {
    const { list } = this.state
    const orders = list.map((order, i) => {
      const { status } = order
      const text = `${['CREATED'].includes(status) ? 'text-black' : 'text-white'}`
      const background = `${status === 'CREATED' ? 'bg-light' : (status === 'CONFIRMED' ? 'bg-primary' : (status === 'CANCELLED' ? 'bg-danger' : 'bg-success'))}`
      return (
        <div key={i} className="col-md-3 col-sm-3 col-lg-3">
          <div className={`card ${text} ${background} mb-3`} style={{ maxWidth: '18rem' }}>
            <div className="card-header">Order #{order.id}</div>
            <div className="card-body">
              <h5 className="card-title">Name: {order.name}</h5>
              <h6 className="card-title">Status: {order.status}</h6>
              <p className="card-text">{order.description}</p>
              {['CREATED', 'CONFIRMED'].includes(order.status) && <div>
                <button onClick={e => this.cancel(order.id)} className="btn btn-danger">Cancel Order</button>
              </div>}
            </div>
          </div>
        </div>
      )
    })
    this.setState({ orders })
  }

  cancel = async id => {
    try {
      const request = await axios.put(`http://${URL}/api`, { id })
      const { data } = request
      const { list } = this.state
      const updatedList = [ ...list ]
      const index = updatedList.findIndex(i => i.id === id)
      updatedList[index].status = data.status
      this.setState({ list: updatedList }, () => this.renderOrders())
    } catch (e) {
      console.log('Error')
    }
  }

  submit = async () => {
    const { name, description, list } = this.state
    if(name !== null && description !== null && name !== '' && description !== '') {
      try {
        const request = await axios.post(`http://${URL}/api`, { name, description })
        const { data } = request
        const updatedList = [ ...list, data ]
        this.setState({ list: updatedList }, () => this.renderOrders())
      } catch (e) {
        console.log('Error')
      }
    }
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="form-card card">
          <div className="card-header">Order form</div>
          <div className="card-body">
            <div className="form-group">
              <label htmlFor="name">Name</label>
              <input type="text" className="form-control" id="name" placeholder="Enter name" onChange={this.update}/>
            </div>
            <div className="form-group">
              <label htmlFor="description">Order</label>
              <input type="text" className="form-control" id="description" placeholder="Enter order" onChange={this.update}/>
            </div>
            <button onClick={this.submit} className="btn btn-primary">Submit</button>
          </div>
        </div>

        <div className="order-cards">
          <div className="row">
            {this.state.orders}
          </div>
        </div>
      </div>
    )
  }
}

export default App
